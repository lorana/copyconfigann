package org.sda.spring.copyconfig;

import org.sda.spring.copyconfig.AppConfig;
import org.sda.spring.copyconfig.copy.Copy;
import org.sda.spring.copyconfig.reader.impl.KeyboardReader;
import org.sda.spring.copyconfig.reader.impl.ScannerReader;
import org.sda.spring.copyconfig.writer.impl.PrinterWriter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

		Copy copy = applicationContext.getBean(Copy.class);
		String message = copy.getReader().read();
		copy.getWriter().write(message);

		Copy copy2 = applicationContext.getBean(Copy.class);
		KeyboardReader keyboardReader = applicationContext.getBean(KeyboardReader.class);
		PrinterWriter printerWriter = applicationContext.getBean(PrinterWriter.class);
		copy2.setReader(keyboardReader);
		copy2.setWriter(printerWriter);
		String message2 = copy2.getReader().read();
		copy2.getWriter().write(message2);


	}
}
