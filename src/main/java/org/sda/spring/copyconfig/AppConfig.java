package org.sda.spring.copyconfig;

import org.sda.spring.copyconfig.copy.Copy;
import org.sda.spring.copyconfig.reader.impl.KeyboardReader;
import org.sda.spring.copyconfig.reader.impl.ScannerReader;
import org.sda.spring.copyconfig.writer.impl.DataBaseWriter;
import org.sda.spring.copyconfig.writer.impl.PrinterWriter;
import org.sda.spring.copyconfig.writer.impl.TextFileWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;

public class AppConfig {

	@Bean
	public KeyboardReader keyboardReader() {
		KeyboardReader keyboardReader = new KeyboardReader();
		return keyboardReader;

	}

	@Bean
	public ScannerReader scannerReader() {
		ScannerReader scannerReader = new ScannerReader();
		return scannerReader;

	}

	@Bean
	public DataBaseWriter databaseWriter() {
		DataBaseWriter databaseWriter = new DataBaseWriter();
		return databaseWriter;

	}

	@Bean
	public PrinterWriter printerWriter() {
		PrinterWriter printerWriter = new PrinterWriter();
		return printerWriter;

	}

	@Bean
	public TextFileWriter textFileWriter() {
		TextFileWriter textFileWriter = new TextFileWriter();
		return textFileWriter;

	}
	
	@Bean
	@Scope("prototype")
	public Copy copy() {
		Copy copy = new Copy(scannerReader());
		copy.setWriter(textFileWriter());
		return copy;

	}
}
