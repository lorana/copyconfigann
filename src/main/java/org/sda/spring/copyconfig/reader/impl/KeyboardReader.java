package org.sda.spring.copyconfig.reader.impl;

import org.sda.spring.copyconfig.reader.Reader;
import org.springframework.stereotype.Component;

public class KeyboardReader implements Reader {

	public String read() {

		return "message from keyboard";
	}
}
