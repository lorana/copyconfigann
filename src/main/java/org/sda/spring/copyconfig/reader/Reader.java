package org.sda.spring.copyconfig.reader;

public interface Reader {
	public String read();

}
