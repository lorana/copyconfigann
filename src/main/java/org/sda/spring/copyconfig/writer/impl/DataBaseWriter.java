package org.sda.spring.copyconfig.writer.impl;

import org.sda.spring.copyconfig.writer.Writer;
import org.springframework.stereotype.Component;

public class DataBaseWriter implements Writer {

	public void write(String message) {
		System.out.println("Writing to DataBase: " + message);

	}
}
