package org.sda.spring.copyconfig.writer.impl;

import org.sda.spring.copyconfig.writer.Writer;
import org.springframework.stereotype.Component;

public class PrinterWriter implements Writer {

	public void write(String message) {

		System.out.println("Printing message to printer: " + message);

	}

}
