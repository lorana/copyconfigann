package org.sda.spring.copyconfig.writer.impl;

import org.sda.spring.copyconfig.writer.Writer;

public class TextFileWriter implements Writer {

	public void write(String message) {
		System.out.println("Writing message to text file: " + message);

	}

}
